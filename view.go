package lionview

import (
	"html/template"
	"sync"
)

// ViewSingleton singleton interface
type ViewSingleton interface {
	GetInstance() *template.Template
	Func(fm template.FuncMap)
}

// View represents web view
type View struct {
	template *template.Template
	funcMap  template.FuncMap
	sync     sync.Once
	files    []string
}

// SubView represents web view using parent view for layouting
type SubView struct {
	template *template.Template
	funcMap  template.FuncMap
	parent   ViewSingleton
	sync     sync.Once
	files    []string
}

// GetInstance returns the singleton instance
func (v *SubView) GetInstance() *template.Template {
	v.sync.Do(func() {
		// get parent view from singleton and clone
		parent, err := v.parent.GetInstance().Clone()

		// check for errors
		if err != nil {
			panic(err)
		}

		// parse template using the layout
		v.template = template.Must(parent.ParseFiles(v.files...))

		// add FuncMap
		if v.funcMap != nil {
			v.template = v.template.Funcs(v.funcMap)
		}
	})

	// return template
	return v.template
}

// GetInstance returns the singleton instance
func (v *View) GetInstance() *template.Template {
	v.sync.Do(func() {
		// parse template
		v.template = template.Must(template.ParseFiles(v.files...))

		// add FuncMap
		if v.funcMap != nil {
			v.template = v.template.Funcs(v.funcMap)
		}
	})

	// return template
	return v.template
}

// Func adds a FuncMap to the view
func (v *View) Func(fm template.FuncMap) {
	v.funcMap = fm
}

// Func adds a FuncMap to the sub view
func (v *SubView) Func(fm template.FuncMap) {
	v.funcMap = fm
}

// NewSubView creates a sub view instance
func NewSubView(v ViewSingleton, file ...string) *SubView {
	return &SubView{
		parent: v,
		files:  file,
	}
}

// NewView creates a view instance
func NewView(file ...string) *View {
	return &View{
		files: file,
	}
}
